package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChainSpell implements Spell {
    private List<Spell> spells;

    public ChainSpell(List<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        for (Spell spell : spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        List<Spell> spellCopy = new ArrayList<>(spells);
        Collections.reverse(spellCopy);
        for (Spell spell: spellCopy) {
            spell.undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
