# Singleton Pattern

## Eager Instantiation
Dengan menggunakan Eager Instantiation, instance dari singleton akan dibuat saat class dibuat 
(class loading).

### Kelebihan
- Simpel dan aman dengan multithreading.

### Kekurangan
- Mungkin saja instance dibuat sebelum dibutuhkan. Hal ini tentu memberatkan jalannya program.

<hr>

## Lazy Instantiation
Dengan menggunakan Lazy Instantiation, instance dari singleton hanya akan dibuat saat dibutuhkan 
yaitu saat method getInstance dipanggil.

### Kelebihan
- Instance dibuat hanya ketika dibutuhkan. Hal ini meringankan jalannya program (mengingat 
  singleton seringkali digunakan untuk menginisiasi resource yang berat)

### Kekurangan
- Multithreading menjadi lebih ribet. Harus mengatasi agar 2 thread tidak menginisiasi pada 
  waktu yang sama.


