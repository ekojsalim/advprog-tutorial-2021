package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MenuTest {
    Menu menu = new InuzumaRamen("ramen");

    @Test
    void getName() {
        assertEquals(menu.getName(), "ramen");
    }

    @Test
    void getNoodle() {
        assertTrue(menu.getNoodle() instanceof Ramen);
    }

    @Test
    void getMeat() {
        assertTrue(menu.getMeat() instanceof Pork);
    }

    @Test
    void getTopping() {
        assertTrue(menu.getTopping() instanceof BoiledEgg);
    }

    @Test
    void getFlavor() {
        assertTrue(menu.getFlavor() instanceof Spicy);
    }
}