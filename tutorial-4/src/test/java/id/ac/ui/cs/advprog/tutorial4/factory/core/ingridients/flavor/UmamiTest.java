package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UmamiTest {

    @Test
    void getDescription() {
        assertEquals("Adding WanPlus Specialty MSG flavoring...", (new Umami()).getDescription());
    }
}