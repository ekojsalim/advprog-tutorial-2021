package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FlowerTest {

    @Test
    void getDescription() {
        assertEquals((new Flower()).getDescription(), "Adding Xinqin Flower Topping...");
    }
}