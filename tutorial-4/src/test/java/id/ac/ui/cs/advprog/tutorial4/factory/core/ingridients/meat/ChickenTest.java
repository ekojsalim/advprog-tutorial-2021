package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChickenTest {

    @Test
    void getDescription() {
        assertEquals("Adding Wintervale Chicken Meat...", (new Chicken()).getDescription());
    }
}