package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoiledEggTest {

    @Test
    void getDescription() {
        assertEquals((new BoiledEgg()).getDescription(), "Adding Guahuan Boiled Egg Topping");
    }
}