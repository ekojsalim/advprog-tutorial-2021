package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

class MenuServiceTest {
    MenuService menuService = new MenuServiceImpl();

    MenuRepository menuRepository = new MenuRepository();

    @Test
    void createMenu() {
        Menu menu = menuService.createMenu("Testy", "InuzumaRamen");

        assertTrue(menuService.getMenus().contains(menu));
    }

    @Test
    void getMenus() {
        assertEquals(4, menuService.getMenus().size());
    }
}