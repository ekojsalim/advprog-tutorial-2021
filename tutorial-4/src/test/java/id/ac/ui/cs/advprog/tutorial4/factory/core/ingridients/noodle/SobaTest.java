package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SobaTest {

    @Test
    void getDescription() {
        assertEquals((new Soba()).getDescription(), "Adding Liyuan Soba Noodles...");
    }
}