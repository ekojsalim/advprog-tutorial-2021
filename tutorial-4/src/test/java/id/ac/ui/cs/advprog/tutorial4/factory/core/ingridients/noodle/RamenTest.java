package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RamenTest {

    @Test
    void getDescription() {
        assertEquals("Adding Inuzuma Ramen Noodles...", (new Ramen()).getDescription());
    }
}