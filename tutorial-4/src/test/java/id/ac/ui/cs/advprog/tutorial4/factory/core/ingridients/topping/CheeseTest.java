package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheeseTest {

    @Test
    void getDescription() {
        assertEquals((new Cheese()).getDescription(), "Adding Shredded Cheese Topping...");
    }
}