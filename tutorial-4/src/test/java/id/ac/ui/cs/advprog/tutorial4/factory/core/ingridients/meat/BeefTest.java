package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BeefTest {

    @Test
    void getDescription() {
        assertEquals("Adding Maro Beef Meat...", (new Beef()).getDescription());
    }
}