package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
    }

    @Override
    public void update() {
        Quest q = guild.getQuest();
        String questType = guild.getQuestType();
        if (questType.equals("E") || questType.equals("D")) {
            this.getQuests().add(q);
        }
    }}
