package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    @Override
    public String defend() {
        return "defffffff";
    }

    @Override
    public String getType() {
        return "Armor";
    }
}
