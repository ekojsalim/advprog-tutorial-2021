package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HumanTransformationTest {
    private Class<?> humanClass;

    @BeforeEach
    public void setup() throws Exception {
        humanClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.HumanTransformation");
    }

    @Test
    public void testHumanHasEncodeMethod() throws Exception {
        Method translate = humanClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testHumanEncodesCorrectly() throws Exception {
        String text = "Bla2";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Bla`";

        Spell result = new HumanTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testHumanHasDecodeMethod() throws Exception {
        Method translate = humanClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testHumanDecodesCorrectly() throws Exception {
        String text = "Bla`";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Bla2";

        Spell result = new HumanTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

}
