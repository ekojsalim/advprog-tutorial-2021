package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {
    private Bow bow;
    boolean isAimShot;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.isAimShot = true;
    }

    @Override
    public String normalAttack() {
        return this.isAimShot ? bow.shootArrow(true) : bow.shootArrow(false);
    }

    @Override
    public String chargedAttack() {
        this.isAimShot = !this.isAimShot;
        return this.normalAttack();
    }

    @Override
    public String getName() {
        return this.bow.getName();
    }

    @Override
    public String getHolderName() {
        return this.bow.getHolderName();
    }
}