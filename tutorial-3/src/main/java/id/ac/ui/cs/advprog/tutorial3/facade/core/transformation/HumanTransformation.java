package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

/*
    Transformasi simple mengubah alfabet kodex ke-3 menjadi `
 */
public class HumanTransformation implements Transformation {
    @Override
    public Spell encode(Spell spell) {
        char toBeReplaced = spell.getCodex().getChar(2);
        String replaced = spell.getText().replace(toBeReplaced, '`');
        return new Spell(replaced, spell.getCodex());
    }

    @Override
    public Spell decode(Spell spell) {
        char toBeRestored = spell.getCodex().getChar(2);
        String replaced = spell.getText().replace('`', toBeRestored);
        return new Spell(replaced, spell.getCodex());
    }
}
