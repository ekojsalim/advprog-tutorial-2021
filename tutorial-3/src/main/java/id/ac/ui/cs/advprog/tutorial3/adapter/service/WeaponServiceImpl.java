package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private BowRepository bowRepository;


    @Override
    public List<Weapon> findAll() {
        List<Weapon> combined = new ArrayList<>();
        combined.addAll(weaponRepository.findAll());
        combined.addAll(spellbookRepository.findAll().stream().map(SpellbookAdapter::new).collect(toList()));
        combined.addAll(bowRepository.findAll().stream().map(BowAdapter::new).collect(toList()));
        return combined;
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = this.weaponRepository.findByAlias(weaponName);
        if (weapon == null) {
            weapon = this.findAll().stream().filter(el -> el.getName().equals(weaponName)).findFirst().orElse(null);
        }
        if (weapon != null) {
            weaponRepository.save(weapon);
            logAttack(weapon, attackType);
        }
    }

    private void logAttack(Weapon weapon, int attackType) {
        String weaponResult = attackType == 0 ? weapon.normalAttack() : weapon.chargedAttack();
        String attackTypeString = attackType == 0 ? "(normal attack)" : "(charged attack)";
        this.logRepository.addLog(String.format("%s attacked with %s %s: %s", weapon.getHolderName(), weapon.getName(), attackTypeString, weaponResult));
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
