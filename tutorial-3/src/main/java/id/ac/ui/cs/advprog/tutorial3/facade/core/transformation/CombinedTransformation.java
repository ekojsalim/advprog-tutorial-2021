package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class CombinedTransformation implements Transformation {
    private List<Transformation> transformationList = new ArrayList<>();

    public CombinedTransformation(List<Transformation> transformationList) {
        this.transformationList = transformationList;
    }

    public CombinedTransformation() {
        transformationList = new ArrayList<>();
        transformationList.add(new CelestialTransformation());
        transformationList.add(new AbyssalTransformation());
        transformationList.add(new HumanTransformation());
    }

    @Override
    public Spell encode(Spell spell) {
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
        for (Transformation transformation : transformationList) {
            spell = transformation.encode(spell);
        }
        return spell;
    }

    @Override
    public Spell decode(Spell spell) {
        ListIterator<Transformation> li = transformationList.listIterator(transformationList.size());
        while (li.hasPrevious()) {
            Transformation transformation = li.previous();
            spell = transformation.decode(spell);
        }

        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());

        return spell;
    }
}
